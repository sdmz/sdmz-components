import React, { Component } from 'react'
import styled from 'styled-components'
import Header from './components/Header/Header'
import Menu from './components/Menu/Menu'
import Footer from './components/Footer/Footer'
import SignUp from './components/SignUp/SignUp'

const Grid = styled.div`
  display: grid;
	grid-template-areas: "header header header"
	                     "sidebar main main"
	                     "footer footer footer";
	grid-template-columns: 240px 1fr;
	grid-template-rows: 40px 1fr 40px;
  min-height: 100vh;

  @media screen and (max-width: 600px) {
    grid-template-areas: "header"
		                     "sidebar"
		                     "main"
		                     "footer";
		grid-template-columns: 100%;
		grid-template-rows: 100px 50px 1fr 40px;
  }
`
const GridHeader = styled.div`
  grid-area: header;
  background: #36424E;
  color: #ffffff;
`
const GridMain = styled.div`
  grid-area: main;
  background: #FFFFFF;
`
const GridSidebar = styled.div`
  grid-area: sidebar;
  background: #A7B9D1;
`
const GridFooter = styled.div`
  grid-area: footer;
  background: #36424E;
  color: #FFFFFF;
`

class App extends Component {
  render() {
    return (
      <Grid>
        <GridHeader>
          <Header />
        </GridHeader>
        <GridMain>
          <SignUp/>
        </GridMain>
        <GridSidebar>
          <Menu />
        </GridSidebar>
        <GridFooter>
          <Footer />
        </GridFooter>
      </Grid>
    );
  }
}

export default App;
