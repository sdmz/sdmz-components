import React from 'react'
import styled from 'styled-components'

const MenuWrapper = styled.div`
    margin: 0 auto;
    padding: 10px;
`

const Menu = () => {
    return (
        <MenuWrapper>
            Menu
        </MenuWrapper>
    )
}

export default Menu