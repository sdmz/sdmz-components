import React, { Component } from 'react'
import styled from 'styled-components'

const SignUpWrapper = styled.div`
    margin: 0 auto;
    padding: 10px;
`

class SignUp extends Component {
    render () {
        return (
            <SignUpWrapper>
                Sign Up
            </SignUpWrapper>

        )
    }
}

export default SignUp