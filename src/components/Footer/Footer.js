import React from 'react'
import styled from 'styled-components'

const FooterWrapper = styled.div`
    margin: 0 auto;
    padding: 10px;
`

const Footer = () => {
    return (
        <FooterWrapper>
            Footer
        </FooterWrapper>
    )
}

export default Footer