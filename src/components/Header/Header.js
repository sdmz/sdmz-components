import React from 'react'
import styled from 'styled-components'

const HeaderWrapper = styled.div`
    margin: 0 auto;
    padding: 10px;
`

const Header = () => {
    return (
        <HeaderWrapper>
            React UI Library
        </HeaderWrapper>
    )
}

export default Header